import java.util.List;
import java.util.Stack;

public class Automato {
	
	private List<Character> alfabeto;
	private String nomeEstadoInicial;
	private List<Estado> estados;
	
		
	public List<Character> getAlfabeto() {
		return alfabeto;
	}
	
	public boolean pertenceAlfabeto(char caracter) {
		for (Character c : alfabeto) {
			if(c.equals(caracter)) {
				return true;
			}
		}
		System.out.println("Mal formação na criação da String. Caracter não pertence ao alfabeto");
		return false;
	}

	public void setAlfabeto(List<Character> alfabeto) {
		this.alfabeto = alfabeto;
	}


	public String getNomeEstadoInicial() {
		return nomeEstadoInicial;
	}

	public void setNomeEstadoInicial(String nomeEstadoInicial) {
		this.nomeEstadoInicial = nomeEstadoInicial;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public Estado getEstado(String nomeEstado) {
		
		for (Estado e : estados) {
			if(e.getNome().equalsIgnoreCase(nomeEstado))
				return e;
		}
		System.out.println("Mal formação na criação do automato. Estado "+ nomeEstado +" inexistente");
		return null;
	}
	
	public boolean contemEstadoFinal() {
		for (Estado e : estados) {
			if(e.isEstadoFinal())
				return true;
		}
		return false;
	}
	
	public boolean reconhece(String str) {

		Estado estado = this.getEstado(this.getNomeEstadoInicial());
		int indexStr = 0; 							// índice do simbolo que está reconhecendo
		int lengthStr = str.trim().length();		// tamanho da String
		char[] chars = str.trim().toCharArray();	// array de chars da String
		Stack<Pair> ramos = new Stack<>();			// pilha com as ramificações que a árvore pode ter
		
		if(!this.contemEstadoFinal()) {
			System.out.println("Esse automato apenas reconhece conjunto vazio");
		}
		
		while (indexStr <= lengthStr) {

			List<String> epsilonEstados = estado.getEstados('$');

			if (indexStr == lengthStr) {  
				
				if(estado.isEstadoFinal()) {
					return true;
					
				} else if (epsilonEstados.isEmpty() && ramos.isEmpty()) {
					return false;
				}
			}
			
			char caracter = ' ';
			if (indexStr < lengthStr) {
				caracter = chars[indexStr];
				if(!this.pertenceAlfabeto(caracter))
					break;
			}
			indexStr++;
			
			List<String> proximosEstados = estado.getEstados(caracter);
			
			if (proximosEstados.isEmpty() && epsilonEstados.isEmpty()) {

				if (!ramos.isEmpty()) {
					Pair ramo = ramos.pop();
					indexStr = ramo.getIndexStr();
					estado = this.getEstado(ramo.getEstado());
				} else {
					return false;
				}

			} else {
				
				for (String s : epsilonEstados) {
					ramos.push(new Pair(indexStr-1, s));
				}
				
				for (String s : proximosEstados) {
					ramos.push(new Pair(indexStr, s));
				}
				
				Pair ramo = ramos.pop();
				indexStr = ramo.getIndexStr();
				estado = this.getEstado(ramo.getEstado());
				
			}
		}
		return false;
	}
}
