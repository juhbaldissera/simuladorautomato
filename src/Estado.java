import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class Estado {
	
	private String nome;
	private List<String[]> funcoesTransicao;
	private boolean estadoFinal;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<String[]> getFuncoesTransicao() {
		return funcoesTransicao;
	}
	public void setFuncoesTransicao(List<String[]> funcoesTransicao) {
		this.funcoesTransicao = funcoesTransicao;
	}
	public boolean isEstadoFinal() {
		return estadoFinal;
	}
	public void setEstadoFinal(boolean estadoFinal) {
		this.estadoFinal = estadoFinal;
	}
	public List<String> getEstados (char caracter) {
		
		List<String> nomesEstados = new ArrayList<>();
		
		for (String[] transicao : funcoesTransicao) {
			if (transicao[0].equalsIgnoreCase(String.valueOf(caracter))) {
				nomesEstados.add(transicao[1]);
			}
		}
		return nomesEstados;
	}
	
}
