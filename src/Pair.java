
public class Pair {

	private int indexStr;
	private String estado;

	
	
	public Pair(Integer indexStr, String estado) {
		this.indexStr = indexStr;
		this.estado = estado;
	}

	public Integer getIndexStr() {
		return this.indexStr;
	}

	public void setIndexStr(Integer indexStr) {
		this.indexStr = indexStr;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
