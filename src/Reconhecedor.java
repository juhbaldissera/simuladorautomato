import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import com.google.gson.Gson;

public class Reconhecedor {


	public static void main(String args[]) {

		Automato automato = new Automato();
		
		// Exemplos de arquivos JSON:
		
		// -> automato.json para a linguagem  L={w|w não contem subcadeia 110
		
		// -> automato_1.json para a linguagem L={w|w pertence {a,b}* e possui substring "aa" e termina com b
		
		// -> automato_2.json para a linguagem  L={w|w é a string "" ou "a" ou "aaa" ou "aaab"}
		
		
		try {
			String strAutomato = Util.readFile("automato.json", "/src/").replace("\n", "").replace("\r", "");

			if (!strAutomato.isEmpty()) {
				Gson gson = new Gson();
				automato = gson.fromJson(strAutomato, Automato.class);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Reconhecedor reconhecedor = new Reconhecedor();
		Scanner input = new Scanner(System.in);

		System.out.println("Digite a string para ser reconhecida");
		String string = input.nextLine();
		
		System.out.println(automato.reconhece(string));
		

	}
}
