import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Util {
	

    public static String readFile (String filename, String dir) throws IOException {
        String path = System.getProperty("user.dir") + dir;
        File file = new File(path + filename);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        try {
                while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                        stringBuilder.append(ls);
                }
                return stringBuilder.toString();
        } catch (IOException ex) {
        	ex.printStackTrace();
        } finally {
                reader.close();
        }
        return null;
    }

}
